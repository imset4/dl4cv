import numpy as np

class Perceptron:
    def __init__(self, N, alpha=0.1):
        # initialize weight matrix and store learning rate
        self.W = np.random.randn(N + 1) / np.sqrt(N)
        self.alpha = alpha

    def step(self, x):
        return 1 if x > 0 else 0

    def fit(self, X, y, epochs=10):
        # insert bias column
        X = np.c_[X, np.ones((X.shape[0]))]

        # loop over epochs
        for epoch in np.arange(0, epochs):
            # loop over each datapoint
            for (x, target) in zip(X, y):
                # get prediction
                p = self.step(np.dot(x, self.W))
                # weight update only if prediction is wrong
                if p != target:
                    error = p - target
                    # update weight matrix
                    self.W -= self.alpha * error * x

    def predict(self, X, addBias=True):
        # ensure input is matrix
        X = np.atleast_2d(X)
        # insert bias column
        if addBias:
            X = np.c_[X, np.ones(X.shape[0])]
        # predict
        return self.step(np.dot(X, self.W))