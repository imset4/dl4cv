# import the necessary packages
# import logging

import numpy as np
import os

class ImageNetHelper:
    def __init__(self, config):
        # store the configuration object
        self.config = config
        # build the label mappings and validation blacklist
        self.labelMappings = self.buildClassLabels()
        self.valBlacklist = self.buildBlackist()

    def buildClassLabels(self):
        # load contents of file that maps the WordNet IDs to integers, then initialize the label mappings dictionary
        rows = open(self.config.WORD_IDS).read().strip().split("\n")
        labelMappings = {}
        # loop over the labels
        for row in rows:
            # split the row into the WordNet ID, label integer, and human readable label
            (wordID, label, hrLabel) = row.split(" ")
            # update the label mappings dictionary using the word ID as the key and the label as the value
            # subtracting `1` from the label since MATLAB is one-indexed while Python is zero-indexed
            labelMappings[wordID] = int(label) - 1
        # return the label mappings dictionary
        return labelMappings

    def buildBlackist(self):
        # load the list of blacklisted image IDs and convert them to a set
        rows = open(self.config.VAL_BLACKLIST).read()
        rows = set(rows.strip().split("\n"))
        # return the blacklisted image IDs
        return rows

    def buildTrainingSet(self):
        # load the contents of the training input file that lists the partial image ID and image number
        # initialize the list of image paths and class labels
        #rows = open(self.config.TRAIN_LIST).read().strip()
        #rows = rows.split("\n")
        paths = []
        labels = []
        # loop over files in directories and create lists
        dirs = [f for f in os.listdir(self.config.IMAGES_PATH) if os.path.isdir(os.path.join(self.config.IMAGES_PATH, f))]
        for d in dirs:
            dirpath = os.path.join(self.config.IMAGES_PATH, d)
            files = [f for f in os.listdir(dirpath) if os.path.isfile(os.path.join(dirpath, f))]
            for f in files:
                imagepath = os.path.sep.join([dirpath, f])
                label = self.labelMappings[d]
                # update the respective paths and label lists
                paths.append(imagepath)
                labels.append(label)

        # return a tuple of image paths and associated integer class labels
        return np.array(paths), np.array(labels)

    def buildValidationSet(self):
        # initialize the list of image paths and class labels
        paths = []
        labels = []
        # logger = logging.getLogger() logger.debugger("")
        # load the contents of the file that lists the partial validation image filenames
        # valFilenames = open(self.config.VAL_LIST).read()
        # valFilenames = valFilenames.strip().split("\n")
        valFilenames = [f for f in os.listdir(self.config.VAL_PATH)
                        if os.path.isfile(os.path.join(self.config.VAL_PATH, f))]
        # load the contents of the file that contains the ground-truth integer class labels for the validation set
        valLabels = open(self.config.VAL_LABELS).read()
        valLabels = valLabels.strip().split("\n")
        # loop over the validation data
        for (partialPath, label) in zip(valFilenames, valLabels):
            # break the row into the partial path and image number
            # (partialPath, imageNum) = row.strip().split(" ")
            # if the image number is in the blacklist set then we should ignore this validation image
            # print("[i] testing ", str(valFilenames.index(partialPath) +1))
            if str(valFilenames.index(partialPath) + 1) in self.valBlacklist:
                #print("found it!")
                continue
            # construct the full path to the validation image, then update the respective paths and labels lists
            path = os.path.sep.join([self.config.VAL_PATH, partialPath])
            paths.append(path)
            labels.append(int(label) - 1)
        # return a tuple of image paths and associated integer class labels
        return np.array(paths), np.array(labels)