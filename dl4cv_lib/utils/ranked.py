# imports
import numpy as np

def rank5_accuracy(preds, labels):
    # init
    rank1 = 0
    rank5 = 0

    # loop over preds and ground truth labels
    for (p, gt) in zip(preds, labels):
        # sort probabilities in descending order
        p = np.argsort(p)[::-1]

        # check if GT is in top-5
        if gt in p[:5]:
            rank5 +=1

        # check if GT is top-1
        if gt == p[0]:
            rank1 +=1

    # compute final accuracies
    rank1 /= float(len(preds))
    rank5 /= float(len(preds))

    # return tuple of accuracies
    return rank1, rank5
