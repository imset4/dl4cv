# import
import imutils
import cv2

class AspectAwarePreprocessor:
    def __init__(self, width, height, inter=cv2.INTER_AREA):
        # store target image width, height, interpolation method when resizing
        self.width = width
        self.height = height
        self.inter = inter

    def preprocess(self, image):
        # get dimensions of image and initialize deltas to use when cropping
        (h, w) = image.shape[:2]
        dW = 0
        dH = 0
        # resize along the smaller dimension
        if w < h:
            image = imutils.resize(image, width=self.width, inter=self.inter)
            dH = int((image.shape[0] - self.height) / 2.0)
        else:
            image = imutils.resize(image, height=self.height, inter=self.inter)
            dW = int((image.shape[1] - self.width) / 2.0)
        # get dimensions again (they changed with resize) and discard (half-)delta each side
        (h, w) = image.shape[:2]
        image = image[dH:h-dH, dW:w-dW]
        # do final resize to ensure fixed required size (against rounding errors above)
        return cv2.resize(image, (self.width, self.height), interpolation=self.inter)