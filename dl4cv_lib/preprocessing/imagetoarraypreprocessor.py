# import
from keras.preprocessing.image import img_to_array

class ImageToArrayPreprocessor:
    def __init__(self, dataFormat=None):
        # store image data format
        self.dataFormat = dataFormat

    def preprocess(self, image):
        # rearrange image dimensions
        return img_to_array(image, data_format=self.dataFormat)