#python train_googlenet.py --checkpoints output/checkpoints -l 1e-3 -e 40
#sleep 5s
#python train_googlenet.py --checkpoints output/checkpoints -m output/checkpoints/epoch_40.hdf5 --start_epoch 40 -l 1e-4 -e 20
#sleep 5s
#python train_googlenet.py --checkpoints output/checkpoints -m output/checkpoints/epoch_60.hdf5 --start_epoch 60 -l 1e-5 -e 10
#sleep 5s
#python timn_rank_accuracy.py -m output/checkpoints/epoch_70.hdf5
python resnet_timn_decay.py -c output/checkpoints -e 75
sleep 5s
python timn_rank_accuracy.py -m output/checkpoints/epoch_75.hdf5