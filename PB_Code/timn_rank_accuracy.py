# import the necessary packages
import timn_config as config
from dl4cv_lib.preprocessing import ImageToArrayPreprocessor
from dl4cv_lib.preprocessing import SimplePreprocessor
from dl4cv_lib.preprocessing import MeanPreprocessor
from dl4cv_lib.utils.ranked import rank5_accuracy
from dl4cv_lib.io import HDF5DatasetGenerator
from tensorflow.keras.models import load_model
import argparse
import json

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-m", "--model", required=True, help="path to *specific* model checkpoint to evaluate")
args = vars(ap.parse_args())

# load the RGB means for the training set
means = json.loads(open(config.DATASET_MEAN).read())

# initialize the image preprocessors
sp = SimplePreprocessor(64, 64)
mp = MeanPreprocessor(means["R"], means["G"], means["B"])
iap = ImageToArrayPreprocessor()

# initialize the testing dataset generator
testGen = HDF5DatasetGenerator(config.TEST_HDF5, 64, preprocessors=[sp, mp, iap], classes=config.NUM_CLASSES)

# load the pre-trained network
print("[INFO] loading model...")
model = load_model(args["model"])

# make predictions on the testing data
print("[INFO] predicting on test data...")
predictions = model.predict_generator(testGen.generator(), steps=testGen.numImages // 64, max_queue_size=10)

# compute the rank-1 and rank-5 accuracies
(rank1, rank5) = rank5_accuracy(predictions, testGen.db["labels"])
print("[INFO] rank-1: {:.2f}%".format(rank1 * 100))
print("[INFO] rank-5: {:.2f}%".format(rank5 * 100))

# close the database
testGen.close()