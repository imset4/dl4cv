# import the necessary packages
import dvc_config as config
# from sklearn.preprocessing import LabelBinarizer
# from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
# from dl4cv_lib.datasets import SimpleDatasetLoader
# from imutils import paths
import numpy as np
from dl4cv_lib.preprocessing import ImageToArrayPreprocessor
from dl4cv_lib.preprocessing import SimplePreprocessor
from dl4cv_lib.preprocessing import PatchPreprocessor
from dl4cv_lib.preprocessing import MeanPreprocessor
from dl4cv_lib.nn.conv import FCHeadNet
from dl4cv_lib.callbacks import TrainingMonitor
from dl4cv_lib.io import HDF5DatasetGenerator
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import Adam
from keras.optimizers import SGD
from keras.applications import ResNet50
from keras.layers import Input
from keras.models import Model
import h5py
import argparse
import json
import os

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
# ap.add_argument("-d", "--dataset", required=True, help="path to input dataset")
ap.add_argument("-m", "--model", required=True, help="path to output model")
ap.add_argument("-b", "--batchsize", type=int, default=128, help="path to output model")
args = vars(ap.parse_args())

# batch size
bs = args["batchsize"]
# construct the image generator for data augmentation
aug = ImageDataGenerator(rotation_range=30, width_shift_range=0.1, height_shift_range=0.1, shear_range=0.2,
                         zoom_range=0.2, horizontal_flip=True, fill_mode="nearest")

# # grab the list of images that we'll be describing, then extract the class label names from the image paths
# print("[INFO] loading images...")
# imagePaths = list(paths.list_images(args["dataset"]))
# classNames = [pt.split(os.path.sep)[-2] for pt in imagePaths]
# classNames = [str(x) for x in np.unique(classNames)]

# load the RGB means for the training set
means = json.loads(open(config.DATASET_MEAN).read())

# initialize the image preprocessors
sp = SimplePreprocessor(224, 224)
pp = PatchPreprocessor(224, 224)
mp = MeanPreprocessor(means["R"], means["G"], means["B"])
iap = ImageToArrayPreprocessor()

# # load the dataset from disk then scale the raw pixel intensities to the range [0, 1]
# sdl = SimpleDatasetLoader(preprocessors=[aap, iap])
# (data, labels) = sdl.load(imagePaths, verbose=500)
# data = data.astype("float") / 255.0

# initialize the training and validation dataset generators
trainGen = HDF5DatasetGenerator(config.TRAIN_HDF5, bs, aug=aug, preprocessors=[pp, mp, iap], classes=2)
valGen = HDF5DatasetGenerator(config.VAL_HDF5, bs, preprocessors=[sp, mp, iap], classes=2)

# # partition the data into 75% training and 25% testing splits
# (trainX, testX, trainY, testY) = train_test_split(data, labels, test_size=0.25, random_state=42)

# # convert the labels from integers to vectors
# trainY = LabelBinarizer().fit_transform(trainY)
# testY = LabelBinarizer().fit_transform(testY)

# load the ResNet50 network, ensuring the head FC layer sets are left off
baseModel = ResNet50(weights="imagenet", include_top=False, input_tensor=Input(shape=(224, 224, 3)))

# initialize the new head of the network, a set of FC layers followed by a softmax classifier
headModel = FCHeadNet.build(baseModel, 2, 256)

# place the head FC model on top of the base model -- this will become the actual model we will train
model = Model(inputs=baseModel.input, outputs=headModel)

# loop over all layers in the base model and freeze them so they will *not* be updated during the training process
for layer in baseModel.layers:
    layer.trainable = False

# compile our model (this needs to be done after our setting our layers to being non-trainable)
print("[INFO] compiling model...")
opt = Adam(lr=1e-3)
model.compile(loss="binary_crossentropy", optimizer=opt, metrics=["accuracy"])

# construct the set of callbacks
path = os.path.sep.join([config.OUTPUT_PATH, "{}.png".format(os.getpid())])
callbacks = [TrainingMonitor(path)]

# train the head of the network for a few epochs (all other layers are frozen)
# this will allow the new FC layers to start to become initialized with actual "learned" values versus pure random
print("[INFO] training head...")
model.fit_generator(trainGen.generator(), steps_per_epoch=trainGen.numImages // bs, validation_data=valGen.generator(),
                    validation_steps=valGen.numImages // bs, epochs=10, max_queue_size=10,
                    callbacks=callbacks, verbose=1)

# # evaluate the network after initialization
# print("[INFO] evaluating after initialization...")
# predictions = model.predict(testX, batch_size=32)
# print(classification_report(testY.argmax(axis=1), predictions.argmax(axis=1), target_names=classNames))

# save the model to disk
print("[INFO] serializing model...")
model.save(args["model"], overwrite=True)

# now that the head FC layers have been trained, unfreeze the final set of CONV layers and make them trainable
# it initially said baseModel.layers[15:]
for layer in baseModel.layers:
    layer.trainable = True

# for the changes to the model to take affect, recompile the model, using SGD with a *very* small learning rate
print("[INFO] re-compiling model...")
opt = SGD(lr=0.001)
model.compile(loss="binary_crossentropy", optimizer=opt, metrics=["accuracy"])

# train the model again, this time fine-tuning *both* the final set of CONV layers along with our set of FC layers
print("[INFO] fine-tuning model...")
model.fit_generator(trainGen.generator(), steps_per_epoch=trainGen.numImages // bs, validation_data=valGen.generator(),
                    validation_steps=valGen.numImages // bs, epochs=10, max_queue_size=10,
                    callbacks=callbacks, verbose=1)

# # evaluate the network on the fine-tuned model
# print("[INFO] evaluating after fine-tuning...")
# predictions = model.predict(testX, batch_size=32)
# print(classification_report(testY.argmax(axis=1), predictions.argmax(axis=1), target_names=classNames))

# save the model to disk again, overwriting
print("[INFO] serializing model...")
model.save(args["model"], overwrite=True)

# close the HDF5 datasets
trainGen.close()
valGen.close()

# test on testing set
# generate one image per batch, to make sure all predictions are obtained
test = HDF5DatasetGenerator(config.TEST_HDF5, 1, preprocessors=[sp, mp, iap], classes=2)
predictions = model.predict_generator(test.generator(), steps=test.numImages)
preds = np.argmax(predictions, axis=1)
# get the test set file
ftest = h5py.File(config.TEST_HDF5)
# generate report
print(classification_report(ftest["labels"], preds, target_names=["cats", "dogs"]))
