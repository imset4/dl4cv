# import the necessary packages
import os

# define the content layer from which feature maps will be extracted
contentLayers = ["block4_conv2"]

styleLayers = ["block1_conv1", "block2_conv1", "block3_conv1", "block4_conv1", "block5_conv1"]

# define the style weight, content weight, and total-variational loss weight
styleWeight = 1e2
contentWeight = 1e4
tvWeight = 20.0

# define the number of epochs to train for along with the steps per each epoch
epochs = 15
stepsPerEpoch = 100

# define the path to the input content image, input style image, final output image
# and path to the directory that will store the intermediate outptus
contentImage = os.path.sep.join(["pics", "jp.jpg"])
styleImage = os.path.sep.join(["pics", "pablo_picasso.jpg"])
finalImage = os.path.sep.join(["pics", "final.jpg"])
intermOutputs = os.path.sep.join(["pics", "intermediate"])