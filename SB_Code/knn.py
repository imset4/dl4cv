# import
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from dl4cv_lib.preprocessing import SimplePreprocessor
from dl4cv_lib.datasets import SimpleDatasetLoader
from imutils import paths
import argparse

# parse
ap = argparse.ArgumentParser()
ap.add_argument('-d', required=True, help='path')
ap.add_argument('-k', type=int, default=4, help='# neighbours')
ap.add_argument('-j', type=int, default=1, help='cores')
ap.add_argument('-v', type=int, default=0, help='verbose')
ap.add_argument('-q', type=float, default=.25, help='test size')
args = vars(ap.parse_args())

# get list
print('[i] loading...')
imagepaths = list(paths.list_images(args['d']))

# load & reshape
sp = SimplePreprocessor(32, 32)
sdl = SimpleDatasetLoader(preprocessors=[sp])
(data, labels) = sdl.load(imagepaths, verbose=args['v'])
data = data.reshape((data.shape[0], 3072))
print('[i] size: {:.1f}MB'.format(data.nbytes / 1024 / 1000.0))

# encode labels
le = LabelEncoder()
labels = le.fit_transform(labels)

# partition data
(trX, tsX, trY, tsY) = train_test_split(data, labels, test_size=args['q'], random_state=42)

# train
print('[i] evaluating kNN...')
model = KNeighborsClassifier(n_neighbors=args['k'], n_jobs=args['j'])
model.fit(trX, trY)
print('[i] fit complete...')
print('classifying with test size {:.0f}%...'.format(args['q'] * 100))
print(classification_report(tsY, model.predict(tsX), target_names=le.classes_))
