# import
import numpy as np
import cv2

# initialise labels and seed
labels = ['dog', 'cat', 'panda']
np.random.seed(1)

# initialise weights and bias
W = np.random.randn(3, 3072)
b = np.random.randn(3)

# load
orig = cv2.imread('beagle.png')
image = cv2.resize(orig, (32, 32)).flatten()

# compute
scores = W.dot(image) + b

# display
for (label, score) in zip(labels, scores):
    print('[i] {}: {:.2f}'.format(label, score))

# draw
cv2.putText(orig, 'label: {}'.format(labels[np.argmax(scores)]),
            (10, 30), cv2.FONT_HERSHEY_SIMPLEX, .9, (0, 255, 0), 2)
cv2.imshow('image', orig)
cv2.waitKey(0)
