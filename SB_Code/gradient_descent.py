# import
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.datasets import make_blobs
import matplotlib.pyplot as plt
import numpy as np
import argparse


def sigmoid_activation(x):
    return 1.0 / (1+np.exp(-x))


def sigmoid_deriv(x):
    return x*(1-x)


def predict(X, W):
    preds = sigmoid_activation(X.dot(W))

    preds[preds <= .5] = 0
    preds[preds > .5] = 1
    return preds


# parse arguments
ap = argparse.ArgumentParser()
ap.add_argument('-e', type=float, default=100, help='# epochs')
ap.add_argument('-a', type=float, default=.01, help='learning rate')
args = vars(ap.parse_args())
# generate 2-class problem with 1000 2D datapoints
(X, y) = make_blobs(n_samples=1000, n_features=2, centers=2, cluster_std=1.5, random_state=1)
y = y.reshape((y.shape[0], 1))
# insert bias
X = np.c_[X, np.ones((X.shape[0]))]
# split data
(trX, tsX, trY, tsY) = train_test_split(X, y, test_size=.5, random_state=42)
# init weights and losses
print('[i] training')
W = np.random.randn(X.shape[1], 1)
losses = []
# loop over epochs
for epoch in np.arange(0, args['e']):
    preds = sigmoid_activation(trX.dot(W))
    loss = np.sum((preds - trY) ** 2)
    losses.append(loss)
    # update GD
    d = (preds - trY) * sigmoid_deriv(preds)
    gradient = trX.T.dot(d)
    W += -args['a'] * gradient
    # print gay update
    if epoch == 0 or (epoch + 1) % 5 == 0:
        print('[i] epoch = {}, loss = {:.7f}'.format(int(epoch + 1), loss))
# evaluate
print('[i] evaluating...')
preds = predict(tsX, W)
print(classification_report(tsY, preds))
# plot test data
plt.style.use('ggplot')
plt.figure()
plt.title('Data')
plt.scatter(tsX[:, 0], tsX[:, 1], marker='o', c=tsY[:, 0], s=30)
# plot loss
plt.style.use('ggplot')
plt.figure()
plt.plot(np.arange(0, args['e']), losses)
plt.title('Losses')
plt.xlabel('epoch')
plt.ylabel('loss')
# needed for pycharm
plt.show()
