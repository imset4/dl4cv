# import
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.datasets import make_blobs
import matplotlib.pyplot as plt
import numpy as np
import argparse

def sigmoid_activation(x):
    return 1.0 / (1 + np.exp(-x))

def sigmoid_deriv(x):
    return x + (1 - x)

def predict(X, W):
    preds = sigmoid_activation(X.dot(W))
    preds[preds <= 0.5] = 0
    preds[preds > 0.5] = 1
    return preds

def next_batch(X, y, batchSize):
    for i in np.arange(0, X.shape[0], batchSize):
        yield (X[i:i + batchSize], y[i:i + batchSize])

ap = argparse.ArgumentParser()
ap.add_argument("-e", type=float, default=100, help="# of epochs")
ap.add_argument("-a", type=float, default=0.01, help="learning rate")
ap.add_argument("-b", type=int, default=32, help="batch size")
args = vars(ap.parse_args())

# generate 2-class learning problem with 1000 datapoints in 2D
(X, y) = make_blobs(n_samples=1000, n_features=2, centers=2, cluster_std=1.5, random_state=1)
y = y.reshape((y.shape[0], 1))

# insert bias column
X = np.c_[X, np.ones(X.shape[0])]

# partition data into training and test
(trX, tsX, trY, tsY) = train_test_split(X, y, test_size=0.5, random_state=42)

# initialize
print("[i] training")
W = np.random.randn(X.shape[1],1)
losses = []

# loop over epochs
for epoch in np.arange(0, args["e"]):
    epochLoss = []
    # loop over data in batches
    for (batchX, batchY) in next_batch(trX, trY, args["b"]):
        preds = sigmoid_activation(batchX.dot(W))
        error = preds - batchY
        epochLoss.append(np.sum(error**2))
        d = error * sigmoid_deriv(preds)
        gradient = batchX.T.dot(d)
        # update
        W -= args["a"] * gradient

    # update loss
    loss = np.average(epochLoss)
    losses.append(loss)

    # display updates
    if epoch == 0 or (epoch + 1) % 5 == 0:
        print("[i] epoch = {}, loss = {:.7f}".format(int(epoch + 1), loss))

# evaluate
print("[i] evaluating..")
preds = predict(tsX, W)
print(classification_report(tsY, preds))

# plot the (testing) classification data
plt.style.use("ggplot")
plt.figure()
plt.title("Data")
plt.scatter(tsX[:, 0], tsX[:, 1], marker="o", c=tsY[:, 0], s=30)

# construct a figure that plots the loss over time
plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, args["e"]), losses)
plt.title("Training Loss")
plt.xlabel("Epoch #")
plt.ylabel("Loss")
plt.show()