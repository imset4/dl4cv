# imports
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from dl4cv_lib.preprocessing import SimplePreprocessor
from dl4cv_lib.datasets import SimpleDatasetLoader
from imutils import paths
import argparse

# argparse
ap = argparse.ArgumentParser()
ap.add_argument("-d", required=True, help="path to input dataset")
args = vars(ap.parse_args())

# get list of image paths
print("[i] loading images..")
imagePaths = list(paths.list_images(args["d"]))

# init image processor, load data from disk, reshape data matrix
sp = SimplePreprocessor(32, 32)
sd1 = SimpleDatasetLoader(preprocessors=[sp])
(data, labels) = sd1.load(imagePaths, verbose=500)
data = data.reshape(data.shape[0], 3072)

# encode labels as integers
le = LabelEncoder()
labels = le.fit_transform(labels)

# partition into train-test .75 / .25
(trX, tsX, trY, tsY) = train_test_split(data, labels, test_size=0.25, random_state=5)

# loop over regularizers
for r in (None, "l1", "l2"):
    # train SGD using softmax loss for 10 epochs
    print("[i] training model with {} regulatization".format(r))
    model = SGDClassifier(loss="log", penalty=r, max_iter=10,
                          learning_rate="constant", tol=1e-3, eta0=0.01, random_state=42)
    model.fit(trX, trY)

    # evaluate classifier
    acc = model.score(tsX, tsY)
    print("[i] {} regularization accuracy: {:.2f}%".format(r, acc * 100))