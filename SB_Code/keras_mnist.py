# import
from sklearn.preprocessing import LabelBinarizer
from sklearn.metrics import classification_report
from keras.models import Sequential
from keras.layers.core import Dense
from keras.optimizers import SGD
from keras.datasets import mnist
from keras import backend as K
import matplotlib.pyplot as plt
import numpy as np
import argparse

# argparse
ap = argparse.ArgumentParser()
ap.add_argument('-o', required=True, help='Path to output loss/accuracy plot')
args = vars(ap.parse_args())

# get MNIST
print('[i] accessing MNIST..')
((trX, trY), (tsX, tsY)) = mnist.load_data()
# flatten images
trX = trX.reshape((trX.shape[0], 28 * 28 * 1))
tsX = tsX.reshape((tsX.shape[0], 28 * 28 * 1))
# scale to range [0,1]
trX = trX.astype("float32") / 255.0
tsX = tsX.astype("float32") / 255.0
# encode labels to vectors
lb = LabelBinarizer()
trY = lb.fit_transform(trY)
tsY = lb.fit_transform(tsY)

# define 784-256-128-10 architecture using Keras
model = Sequential()
model.add(Dense(256, input_shape=(784,), activation="sigmoid"))
model.add(Dense(128, activation="sigmoid"))
model.add(Dense(10, activation="softmax"))
# train
print('[i] training..')
sgd = SGD(0.01)
model.compile(loss="categorical_crossentropy", optimizer=sgd, metrics=["accuracy"])
H = model.fit(trX, trY, validation_data=(tsX, tsY), epochs=100, batch_size=128)
# evaluate
print('[i] evaluating network..')
predictions = model.predict(tsX, batch_size=128)
print(classification_report(tsY.argmax(axis=1), predictions.argmax(axis=1),
                            target_names=[str(x) for x in lb.classes_]))
# plot
plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, 100), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, 100), H.history["val_loss"], label="val_loss")
plt.plot(np.arange(0, 100), H.history["acc"], label="train_acc")
plt.plot(np.arange(0, 100), H.history["val_acc"], label="val_acc")
plt.title("Training Loss and Accuracy")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend()
plt.savefig(args["o"])