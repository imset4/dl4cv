# import
from dl4cv_lib.nn import NeuralNetwork
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn import datasets

# load MNIST and apply min/max scaling to bring pixels to [0,1]
print('[i] loading MNIST..')
digits = datasets.load_digits()
data = digits.data.astype("float")
data = (data - data.min()) / (data.max() - data.min())
print('[i] samples: {}, dim: {}'.format(data.shape[0], data.shape[1]))

# train test split at .75/.25
(trX, tsX, trY, tsY) = train_test_split(data, digits.target, test_size=0.25)

# convert labels from integers to vectors
trY = LabelBinarizer().fit_transform(trY)
tsY = LabelBinarizer().fit_transform(tsY)

# train
print('[i] training..')
nn = NeuralNetwork([trX.shape[1], 32, 16, 10])
print('[i] {}'.format(nn))
nn.fit(trX, trY, epochs=1000)

# evaluate
print('[i] evaluating network..')
predictions = nn.predict(tsX)
predictions = predictions.argmax(axis=1)
print(classification_report(tsY.argmax(axis=1), predictions))