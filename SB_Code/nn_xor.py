# import
from dl4cv_lib.nn import NeuralNetwork
import numpy as np

# define dataset
X = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
y = np.array([[0], [1], [1], [0]])

# define network and fit
nn = NeuralNetwork([2,2,1], alpha=0.5)
nn.fit(X, y, epochs=20000)

# show predictions
for (x, target) in zip(X, y):
    pred = nn.predict(x)[0][0]
    step = 1 if pred > 0.5 else 0
    print("[i] data = {}, GT = {}, pred = {}, step = {}".format(x, target[0], pred, step))