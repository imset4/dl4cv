# import
from dl4cv_lib.nn import Perceptron
import numpy as np
import argparse

# argparse
ap = argparse.ArgumentParser()
ap.add_argument("-op", required=True, help='AND / OR')
args = vars(ap.parse_args())

# construct OR dataset
X = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
if args["op"] == "or":
    y = np.array([[0], [1], [1], [1]])
elif args["op"] == "and":
    y = np.array([[0], [0], [0], [1]])
elif args["op"] == "xor":
    y = np.array([[0], [1], [1], [0]])

# define perceptron and train
print('[i] training perceptron..')
p = Perceptron(X.shape[1], alpha=0.1)
p.fit(X, y, epochs=20)

# evaluate perceptron
print('[i] testing perceptron..')
for (x, target) in zip(X, y):
    pred = p.predict(x)
    print('[i] data = {}, ground_truth = {}, pred = {}'.format(x, target[0], pred))